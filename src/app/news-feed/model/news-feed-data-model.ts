export class NewsFeedDataModel {
  id: string;
  source: SourceModel;
  categories: [];
  symbols: [];
  markets: [];
  title: string;
  content: string;
  description: string;
  url: string;
  imageUrl: string;
  publishedAt: string;
  industries: [];
  sectors: []
}

export class SourceModel {
  id: string;
  name: string;
}
