import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {NewsFeedDataModel} from "../model/news-feed-data-model";

@Injectable({
  providedIn: 'root'
})
export class NewsFeedService {

  constructor(
    private http: HttpClient
  ) { }

  getNewsFeedList(): Observable<Array<NewsFeedDataModel>> {
    return this.http.get<Array<NewsFeedDataModel>>('/assets/main-content.json');
  }
}
