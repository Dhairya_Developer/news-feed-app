import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsFeedRoutingModule } from './news-feed-routing.module';
import { NewsFeedComponent } from './news-feed.component';
import {FormsModule} from "@angular/forms";
import {CoreModule} from "../core/core.module";
import {NgSelectModule} from "@ng-select/ng-select";


@NgModule({
  declarations: [NewsFeedComponent],
  imports: [
    CommonModule,
    NewsFeedRoutingModule,
    FormsModule,
    CoreModule,
    NgSelectModule
  ]
})
export class NewsFeedModule { }
