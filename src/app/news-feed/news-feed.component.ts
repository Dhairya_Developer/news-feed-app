import { Component, OnInit } from '@angular/core';
import {NewsFeedDataModel, SourceModel} from "./model/news-feed-data-model";
import {NewsFeedService} from "./service/news-feed.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.css']
})
export class NewsFeedComponent implements OnInit {
  dataList: Array<NewsFeedDataModel>;
  filterDataList: Array<NewsFeedDataModel>;
  searchText: any;

  sourceList: Array<SourceModel>;
  selectedSource: SourceModel;

  constructor(
    private newsFeedService: NewsFeedService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getNewsFeedList();
  }

  private getNewsFeedList() {
    this.newsFeedService.getNewsFeedList().subscribe((response) => {
      this.dataList = new Array<NewsFeedDataModel>();
      this.dataList = JSON.parse(JSON.stringify(response));
      this.filterDataList = JSON.parse(JSON.stringify(response));
      this.getSourceList();
    });
  }

  onClickReadMore(url: string) {
    this.router.navigate([]).then(() => window.open(url, '_blank'));
  }

  private getSourceList() {
    this.sourceList = new Array<SourceModel>();
    if (this.dataList && this.dataList.length !== 0) {
      let map = new Map<string, SourceModel>();
      this.dataList.forEach((ls) => {
        if (ls.source && ls.source.id) {
          if (!map.has(ls.source.id.toLowerCase())) {
            map.set(ls.source.id, ls.source);
          }
        }
      });
      this.sourceList = Array.from(map.values());
    }
  }

  onSelectSource() {
    this.dataList = this.filterDataList;
    if (this.selectedSource) {
      this.dataList = this.dataList.filter((news) => {
        if (news.source && news.source.id) return news.source.id.toLowerCase() === this.selectedSource.id.toLowerCase();
      });
    }
  }
}
