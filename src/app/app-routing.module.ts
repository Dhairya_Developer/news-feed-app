import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'NewsFeed',
    pathMatch: 'full'
  },
  {
    path: 'NewsFeed',
    loadChildren: () => import('./news-feed/news-feed.module').then(m => m.NewsFeedModule)
  },
  {
    path: '**',
    loadChildren: () => import('./news-feed/news-feed.module').then(m => m.NewsFeedModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
